package com.example.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.MyServie;
/**
 * one
 * @author peter
 *
 */
@RestController
public class MyConroller {
	
	@Resource
	private MyServie service;
	

	@RequestMapping("/hystrixtest")
	public String test(){
		service.operate();
		return "hystrix";
	}

}
