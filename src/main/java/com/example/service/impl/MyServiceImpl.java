package com.example.service.impl;

import org.springframework.stereotype.Service;

import com.example.service.MyServie;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class MyServiceImpl implements MyServie {

	/*@HystrixCommand注解常用属性
	fallbackMethod 降级方法
	commandProperties 普通配置属性，可以配置HystrixCommand对应属性，例如采用线程池还是信号量隔离、熔断器熔断规则等等
	ignoreExceptions 忽略的异常，默认HystrixBadRequestException不计入失败
	groupKey() 组名称，默认使用类名称
	commandKey 命令名称，默认使用方法名
	*/
	@HystrixCommand(fallbackMethod = "fallback")
	public boolean operate() {
		int num = (int) (Math.random() * 10);
		char[] a = { 'a', 'b', 'c' };
		System.out.println(a[num]);// 抛出异常时会执行fallback降级方法
		return false;
	}

	public boolean fallback() {
		System.out.println("fallback:outIndexOf:a[]");
		return true;
	}

}
