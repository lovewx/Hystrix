package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * 断路器：出现问题代码后可执行失败方法
 * @author Peter
 * @EnableHystrix 或 @EnableCircuitBreaker 表示开启断路器
 * @EnableHystrixDashboard 表示开启断路器监听器:
 * 		如何进入监听页面，参考：http://blog.csdn.net/liaokailin/article/details/51339357
 * @HystrixCommand 表示该方法使用断路
 * 
 */
@EnableHystrix 
@EnableHystrixDashboard
@SpringBootApplication
public class HystrixApplication {

	public static void main(String[] args) {
		SpringApplication.run(HystrixApplication.class, args);
	}
}
